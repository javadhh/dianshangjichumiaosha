package com.xcl.seckill.error;

public enum EmBusinessError implements  CommonError {
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    USER_NOT_EXIST(20001,"用户不存在"),
    UNKNOW_ERROR(10002,"未知错误"),
    USER_LOGIN_FAIL(20002,"用户手机号或密码错误"),
    STOCK_NOT_ENOUGH(30001,"库存不足"),
    USER_NOT_LOGIN(20003,"用户还未登录")
    ;

    EmBusinessError(int errCode,String errMsg) {
        this.errMsg = errMsg;
        this.errCode = errCode;
    }
    public  int errCode;
    public String errMsg;


    @Override
    public int getErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg ;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.errMsg=errMsg;
        return this;
    }
}
