package com.xcl.seckill.response;

import lombok.Getter;
import lombok.Setter;



@Setter
@Getter
public class CommonReturnType {
    private String status;
    private Object data;
    public  static CommonReturnType create(Object result){
        return  CommonReturnType.create(result,"success");
    }
    public static CommonReturnType create(Object result,String status){
        CommonReturnType type=new CommonReturnType();
        type.setStatus(status);
        type.setData(result);
        return type;
    }
}
