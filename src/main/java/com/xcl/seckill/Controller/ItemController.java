package com.xcl.seckill.Controller;

import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.response.CommonReturnType;
import com.xcl.seckill.service.ItemService;
import com.xcl.seckill.service.domain.ItemModel;
import com.xcl.seckill.viewObject.ItemVo;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@CrossOrigin(allowCredentials = "true",allowedHeaders = "*")
public class ItemController  extends BaseController {
    @Autowired
    private ItemService itemService;
    @ResponseBody
    @RequestMapping(value = "/create",method = {RequestMethod.POST},consumes ={ CONTENT_TYPE_FORMED})
    private CommonReturnType createItem(@RequestParam(name = "title") String title,
                                        @RequestParam(name = "description") String description,
                                        @RequestParam(name = "price") BigDecimal price,
                                        @RequestParam(name = "stock") Integer stock,
                                        @RequestParam(name = "imgUrl") String imgUrl) throws BusinessException {
        ItemModel itemModel = new ItemModel();
        itemModel.setTitle(title);
        itemModel.setDescription(description);
        itemModel.setPrice(price);
        itemModel.setStock(stock);
        itemModel.setImgUrl(imgUrl);
        ItemModel ReturnItem = itemService.createItem(itemModel);
        ItemVo  itemVo=converVofromModel(ReturnItem);
        return  CommonReturnType.create(itemVo);
    }
    @ResponseBody
    @RequestMapping(value = "/get",method = {RequestMethod.GET})
    private  CommonReturnType getItem(@RequestParam(name = "id")Integer id){
        ItemModel itemModel=itemService.getItemById(id);
        ItemVo itemVo=converVofromModel(itemModel);

        return CommonReturnType.create(itemVo);
    }
    @ResponseBody
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    private CommonReturnType list(){
        List<ItemModel> itemModelList=itemService.listItem();
     List<ItemVo> itemVoList=  itemModelList.stream().map(itemModel -> {
            ItemVo itemVo=converVofromModel(itemModel);
            return itemVo;
        }).collect(Collectors.toList());
     return CommonReturnType.create(itemVoList);
    }
    private ItemVo converVofromModel(ItemModel itemModel) {
        if (itemModel == null) {
            return null;
        }
        ItemVo itemVo = new ItemVo();
        BeanUtils.copyProperties(itemModel, itemVo );
        if (itemModel.getPromoModel()!=null){
            itemVo.setPromoStatus(itemModel.getPromoModel().getStatus());
            itemVo.setPromoId(itemModel.getPromoModel().getId());
            itemVo.setStartDate(itemModel.getPromoModel().getStartDate().toString(org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")));
            itemVo.setPromoPrice(itemModel.getPromoModel().getPromoItemPrice()
            );
        }else{
            itemVo.setPromoStatus(0);
        }

        return itemVo;
    }
}
