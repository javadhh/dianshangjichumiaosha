package com.xcl.seckill.Controller;

import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.error.EmBusinessError;
import com.xcl.seckill.response.CommonReturnType;
import com.xcl.seckill.service.OrderService;
import com.xcl.seckill.service.domain.OrderModel;
import com.xcl.seckill.service.domain.UserModel;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@CrossOrigin(allowCredentials = "true",allowedHeaders = "*")
public class OrderController extends BaseController {
    @Autowired
    OrderService orderService;
    @Autowired
    HttpServletRequest request;

    @ResponseBody
    @RequestMapping(value = "/createorder",method = {RequestMethod.POST},consumes ={ CONTENT_TYPE_FORMED})
    public CommonReturnType createOrder(@RequestParam(name = "itemId") Integer itemId,
                                        @RequestParam(name = "amount")Integer amount,
                                        @RequestParam(name = "promoId",required =false)Integer promoId)throws BusinessException {
 Boolean islogin= (Boolean)request.getSession().getAttribute("IS_LOGIN");
 if(islogin==null||!islogin.booleanValue()){
     throw new BusinessException(EmBusinessError.USER_NOT_LOGIN,"用户还没登录，不能下单");
 }
        UserModel userModel=(UserModel)request.getSession().getAttribute("LOGIN_USER");
 System.out.println(userModel.getId());

        OrderModel orderModel=orderService.createOrder(userModel.getId(),itemId,promoId,amount);

     return  CommonReturnType.create(null);
    }
}
