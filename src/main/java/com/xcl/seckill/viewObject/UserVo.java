package com.xcl.seckill.viewObject;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserVo {
    private Integer id;
    private String name;
    private Byte gender;
    private Integer age;
    private String telphone;
}
