package com.xcl.seckill.viewObject;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
@Setter
@Getter
public class ItemVo {
    private Integer id;
    // 商品名称
    private String title;

    // 商品价格
    private BigDecimal price;

    // 商品库存
    private Integer stock;

    // 商品描述
    private String description;

    // 商品销量
    private Integer sales;

    private String imgUrl;
    private  Integer promoStatus;
    private  BigDecimal promoPrice;
    private  Integer promoId;
    private String  startDate;
}
