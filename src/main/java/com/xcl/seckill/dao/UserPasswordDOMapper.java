package com.xcl.seckill.dao;

import com.xcl.seckill.model.UserPasswordDO;
import com.xcl.seckill.model.UserPasswordDOExample;
import org.apache.ibatis.annotations.Param;

public interface UserPasswordDOMapper {
    long countByExample(UserPasswordDOExample example);
   UserPasswordDO findPassword(Integer userId);
   UserPasswordDO selectByUserId(Integer userId);
    int insert(UserPasswordDO record);

    int insertSelective(UserPasswordDO record);

    UserPasswordDO selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserPasswordDO record, @Param("example") UserPasswordDOExample example);

    int updateByExample(@Param("record") UserPasswordDO record, @Param("example") UserPasswordDOExample example);

    int updateByPrimaryKeySelective(UserPasswordDO record);

    int updateByPrimaryKey(UserPasswordDO record);
}