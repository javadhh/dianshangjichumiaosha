package com.xcl.seckill.dao;

import com.xcl.seckill.model.UserDO;
import com.xcl.seckill.model.UserDOExample;
import org.apache.ibatis.annotations.Param;

public interface UserDOMapper {
    long countByExample(UserDOExample example);

    int insert(UserDO record);

    int insertSelective(UserDO record);

    UserDO findBYId(Integer id);
    UserDO selectBytelphone(String telphone);
    int updateByExampleSelective(@Param("record") UserDO record, @Param("example") UserDOExample example);

    int updateByExample(@Param("record") UserDO record, @Param("example") UserDOExample example);

    int updateByPrimaryKeySelective(UserDO record);

    int updateByPrimaryKey(UserDO record);
}