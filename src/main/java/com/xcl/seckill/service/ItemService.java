package com.xcl.seckill.service;

import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.service.domain.ItemModel;

import java.util.List;

public interface ItemService {
    //创建商品
    ItemModel createItem(ItemModel itemModel) throws BusinessException;
    //商品浏览
    List<ItemModel> listItem();
    //商品详情浏览
    ItemModel getItemById(Integer id);
    boolean decreateStock(Integer itemId,Integer amount) throws BusinessException;
    void  increase(Integer itemId,Integer amount) throws BusinessException;
}
