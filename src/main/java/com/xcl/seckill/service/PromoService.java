package com.xcl.seckill.service;

import com.xcl.seckill.model.PromoDO;
import com.xcl.seckill.service.domain.PromoModel;

public interface PromoService {
    PromoModel   getPromoByItemId(Integer itemId);
}
