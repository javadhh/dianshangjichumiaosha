package com.xcl.seckill.service.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
@Setter
@Getter
public class OrderModel {
    private String  id;

    private Integer userId;
    private Integer itemId;
    private BigDecimal itemPrice;
    private Integer promoId;
    private Integer amount;
    private BigDecimal orderPrice;

}
