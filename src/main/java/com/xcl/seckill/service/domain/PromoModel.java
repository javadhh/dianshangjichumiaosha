package com.xcl.seckill.service.domain;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Date;
@Setter
@Getter
public class PromoModel {
    private Integer id;
    private String PromoName;
    private  Integer status;
    private DateTime startDate;
    private DateTime endDate;
    private Integer itemId;
    private BigDecimal promoItemPrice;
}
