package com.xcl.seckill.service.Impl;

import com.xcl.seckill.dao.ItemDOMapper;
import com.xcl.seckill.dao.ItemStockDOMapper;
import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.error.EmBusinessError;
import com.xcl.seckill.model.ItemDO;
import com.xcl.seckill.model.ItemStockDO;
import com.xcl.seckill.service.ItemService;
import com.xcl.seckill.service.PromoService;
import com.xcl.seckill.service.domain.ItemModel;
import com.xcl.seckill.service.domain.PromoModel;
import com.xcl.seckill.validator.ValidationResult;
import com.xcl.seckill.validator.ValidatorImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ValidatorImpl validator;
    @Autowired
    private ItemDOMapper itemDOMapper;
    @Autowired
    private ItemStockDOMapper itemStockDOMapper;
    @Autowired
    PromoService promoService;
    private ItemStockDO coverItemStockDofromItemModel(ItemModel itemModel){
        if (itemModel==null){
            return null;
        }
        ItemStockDO itemStockDO=new ItemStockDO();
    itemStockDO.setItemId(itemModel.getId());
    itemStockDO.setStock(itemModel.getStock());
        return itemStockDO;
    }
    private ItemDO converItemDofromItemModel(ItemModel itemModel){
        if (itemModel==null){
            return null;
        }
        ItemDO itemDo=new ItemDO();
        BeanUtils.copyProperties(itemModel,itemDo);
        return itemDo;
    }
    @Override
    @Transactional
    public ItemModel createItem(ItemModel itemModel) throws BusinessException {
        ValidationResult validationResult=validator.validation(itemModel);
        if(validationResult.isHasErrors()){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,validationResult.getErrMsg());
        }
        ItemDO itemDo=this.converItemDofromItemModel(itemModel);
        itemDOMapper.insertSelective(itemDo);
   itemModel.setId(itemDo.getId());
   ItemStockDO itemStockDO=this.coverItemStockDofromItemModel(itemModel);
itemStockDOMapper.insertSelective(itemStockDO);
        return this.getItemById(itemModel.getId());
    }

    @Override
    public List<ItemModel> listItem() {
        List<ItemDO> itemDOList=itemDOMapper.itemList();
  List<ItemModel> itemModelList  =      itemDOList.stream().map(itemDO -> {
            ItemStockDO itemStockDO= itemStockDOMapper.selectByItemId(itemDO.getId());
            ItemModel itemModel=converModelformdataObject(itemDO,itemStockDO);
            return itemModel;
        }).collect(Collectors.toList());
        return itemModelList;
    }

    @Override
    public ItemModel getItemById(Integer id) {
        ItemDO itemDO=itemDOMapper.selectByPrimaryKey(id);
        if (itemDO==null){
            return null;
        }
        ItemStockDO itemStockDO=itemStockDOMapper.selectByItemId(itemDO.getId());
        ItemModel itemModel=converModelformdataObject(itemDO,itemStockDO);
        PromoModel promoModel =promoService.getPromoByItemId(itemModel.getId());
        if (promoModel!=null&&promoModel.getStatus().intValue()!=3){
            itemModel.setPromoModel(promoModel);
        }
        return itemModel;
    }

    @Override
    @Transactional
    public boolean decreateStock(Integer itemId, Integer amount) throws BusinessException {
        int affectedRow=itemStockDOMapper.decreaseStock(itemId,amount);
        if(affectedRow>0){
            return  true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public void increase(Integer itemId, Integer amount) throws BusinessException {
        itemDOMapper.increaseSales(itemId,amount);

    }

    private ItemModel converModelformdataObject(ItemDO itemDO,ItemStockDO itemStockDO){
        ItemModel itemModel=new ItemModel();
        BeanUtils.copyProperties(itemDO,itemModel);
        itemModel.setStock(itemStockDO.getStock());
        return itemModel;
    }
}
