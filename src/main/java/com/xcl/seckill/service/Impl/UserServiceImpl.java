package com.xcl.seckill.service.Impl;

import com.xcl.seckill.dao.UserDOMapper;
import com.xcl.seckill.dao.UserPasswordDOMapper;
import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.error.EmBusinessError;
import com.xcl.seckill.model.UserDO;
import com.xcl.seckill.model.UserPasswordDO;
import com.xcl.seckill.service.UserService;
import com.xcl.seckill.service.domain.UserModel;
import com.xcl.seckill.validator.ValidationResult;
import com.xcl.seckill.validator.ValidatorImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.ValidationEvent;
import java.util.DuplicateFormatFlagsException;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDOMapper userDOMapper;
    @Autowired
    UserPasswordDOMapper userPasswordDOMapper;
    @Autowired
    ValidatorImpl validator;
    @Override
    public UserModel getUserById(Integer id) {
        UserDO userDO=userDOMapper.findBYId(id);
        if(userDO==null){
            return null;
        }
        UserPasswordDO userPasswordDO=userPasswordDOMapper.findPassword(userDO.getId());
            return  coverFromDataObject(userDO,userPasswordDO);

    }

    @Override
    @Transactional
    public void register(UserModel userModel) throws BusinessException {
        if(userModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidationResult validationResult=validator.validation(userModel);
        if (validationResult.isHasErrors()){
            throw  new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,validationResult.getErrMsg());
        }

        UserDO userDO=coverFromDataObject(userModel);
        try {
            userDOMapper.insertSelective(userDO);

        }catch (DuplicateKeyException ex){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"手机号已注册");
        }
        userModel.setId(userDO.getId());
        UserPasswordDO userPasswordDO=coverPassObject(userModel);
        userPasswordDOMapper.insertSelective(userPasswordDO);
    return;
    }

    @Override
    public UserModel validataLogin(String telphone, String encrptPassword) throws BusinessException {
       UserDO userDO=   userDOMapper.selectBytelphone(telphone);
       if (userDO==null){
           throw new BusinessException(EmBusinessError.USER_LOGIN_FAIL);
       }
       UserPasswordDO userPasswordDO= userPasswordDOMapper.selectByUserId(userDO.getId());
       UserModel userModel=coverFromDataObject(userDO,userPasswordDO);
       if(!StringUtils.equals(encrptPassword,userModel.getEncrptPassword())){
    throw  new BusinessException(EmBusinessError.USER_LOGIN_FAIL);
       }
       return userModel;
    }

    private UserPasswordDO coverPassObject(UserModel userModel){
        if (userModel==null){
            return null;
        }
        UserPasswordDO userPasswordDO=new UserPasswordDO();
        userPasswordDO.setEncrptPassword(userModel.getEncrptPassword());
        userPasswordDO.setUserId(userModel.getId());
        return userPasswordDO;
    }
    private UserDO coverFromDataObject(UserModel userModel){
        if (userModel==null){
            return null;
        }
        UserDO userDO=new UserDO();
        BeanUtils.copyProperties(userModel,userDO);
        return userDO;
    }

    private  UserModel coverFromDataObject(UserDO userDO , UserPasswordDO userPasswordDO){
        if (userDO == null) {
            return  null;
        }
        UserModel userModel=new UserModel();
        BeanUtils.copyProperties(userDO,userModel);
        if(userPasswordDO!=null) {
            userModel.setEncrptPassword(userPasswordDO.getEncrptPassword());
        }
        return  userModel;
    }
}
