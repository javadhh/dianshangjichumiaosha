package com.xcl.seckill.service;

import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.model.OrderDO;
import com.xcl.seckill.service.domain.OrderModel;

public interface OrderService {
    OrderModel createOrder(Integer userId,Integer itemId,Integer promoId,Integer amount) throws BusinessException;
}
