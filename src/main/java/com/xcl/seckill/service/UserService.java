package com.xcl.seckill.service;

import com.xcl.seckill.error.BusinessException;
import com.xcl.seckill.service.domain.UserModel;

public interface UserService {

     UserModel getUserById(Integer id);
      void   register(UserModel userModel) throws BusinessException;
      UserModel  validataLogin(String telphone ,String encrptPassword) throws BusinessException;
}
